# IDS721_week3

This project aims at creating an S3 Bucket using CDK with AWS CodeWhisperer

## Initial project with CodeWhisperer
```
mkdir [folder-name]
cd [folder-name]

cdk init app --language python
source .venv/bin/activate

pip install -r requirements.txt
```

## Deploy
```
cdk synth
cdk deploy
```

![image](WX20240214-232526@2x.png)

## S3 pack & Version & Encryption
![image](2041707971388_.pic_hd.jpg)
![image](2051707971398_.pic.jpg)
![image](2061707971412_.pic.jpg)






