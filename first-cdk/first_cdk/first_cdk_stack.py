import aws_cdk as cdk
import aws_cdk.aws_s3 as s3

from aws_cdk import (
    # Duration,
    Stack,
    # aws_sqs as sqs,
)
from constructs import Construct

class FirstCdkStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)
        ## create an s3 bucket
        bucket = s3.Bucket(
                self, 
                "MyBucket",
                bucket_name="ids721",
                versioned=True
            )
        
